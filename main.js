import { createBoard, playMove } from "./connect4.js";

function initGame(websocket) {
  websocket.addEventListener("open", () => {
    // Envoie un événement "init" en fonction de la personne qui se connecte.
    const params = new URLSearchParams(window.location.search);
    let event = { type: "init" };
    if (params.has("join")) {
      // Un second joueur rejoint une partie existante
      event.join = params.get("join");
    } else {
      // Le 1er joueur crée la partie
    }
    websocket.send(JSON.stringify(event));
  });
}

function showMessage(message) {
  window.setTimeout(() => window.alert(message), 50);
}

function receiveMoves(board, websocket) {
  websocket.addEventListener("message", ({ data }) => {
    const event = JSON.parse(data);
    switch (event.type) {
      case "init":
        // Créer un lien pour inviter le second joueur
        document.querySelector(".join").href = "?join=" + event.join;
        break;
      case "play":
        // Afficher le coup joué avec playMove de connect4.js
        playMove(board, event.player, event.column, event.row);
        break;
      case "win":
        showMessage(`Joueur ${event.player} à gagné !`);
        // Game over ! On ferme la connexion.
        websocket.close(1000);
        break;
      case "error":
        // Afficher le message envoyé par le serveur
        showMessage(event.message);
        break;
      default:
        throw new Error(`Unsupported event type: ${event.type}.`);
    }
  });
}

function sendMoves(board, websocket) {
  // Fonction qui envoie le clic au serveur
  board.addEventListener("click", ({ target }) => {
    // Je récupére la colonne cliquée
    const column = target.dataset.column;
    if (column === undefined) {
      // J'ignore les clics hors des colonnes
      return;
    }
    const event = {
      type: "play",
      // je convertit le N° de colonne en un interger en base 10
      column: parseInt(column, 10),
    };
    // J'envoie le message "play" au serveur aprés l'avoir linéarisé 
    websocket.send(JSON.stringify(event));
  });
}

function getWebSocketServer() {
  if (window.location.host === "connect4.arrobe.fr:8000") {
    return "wss://connect4.arrobe.fr/";
  } else if (window.location.host === "localhost:8000") {
    return "ws://localhost:8001/";
  } else {
    throw new Error(`Hote inconu: ${window.location.host}`);
  }
}

window.addEventListener("DOMContentLoaded", () => {
  // Dés que le DOM est chargé, on y ajoute le tableau de jeu
  const board = document.querySelector(".board");
  createBoard(board);

  // Ouvrir la connexion WebSocket & enregistrer les événements
  const websocket = new WebSocket(getWebSocketServer());
  initGame(websocket);
  receiveMoves(board, websocket);
  sendMoves(board, websocket);
});